/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OW_H_
#define OW_H_

// OneWire commands
#define SKIPROM         0xCC
#define STARTCONVO      0x44  // Tells device to take a temperature reading and put it on the scratchpad
#define COPYSCRATCH     0x48  // Copy EEPROM
#define READSCRATCH     0xBE  // Read EEPROM
#define WRITESCRATCH    0x4E  // Write to EEPROM
#define RECALLSCRATCH   0xB8  // Reload from last known
#define READPOWERSUPPLY 0xB4  // Determine if device needs parasite power
#define ALARMSEARCH     0xEC  // Query bus for devices with an alarm condition

// Scratchpad locations
#define TEMP_LSB        0
#define TEMP_MSB        1
#define HIGH_ALARM_TEMP 2
#define LOW_ALARM_TEMP  3
#define CONFIGURATION   4
#define INTERNAL_BYTE   5
#define COUNT_REMAIN    6
#define COUNT_PER_C     7
#define SCRATCHPAD_CRC  8

const static uint8_t PROGMEM crc8_table[] = {
    0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
  157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
   35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
  190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
   70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
  219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
  101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
  248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
  140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
   17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
  175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
   50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
  202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
   87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
  233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
  116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
};

static uint8_t ow_crc8(const uint8_t* addr, uint8_t len) {
	uint8_t crc = 0;

	for ( uint8_t c = 0; c < len; ++c ) {
		crc = pgm_read_byte(crc8_table + (crc ^ *addr++));
	}

	return crc;
}

#define ow_low() (DDR_TEMP |= MASK_TEMP)
#define ow_high() (DDR_TEMP &= ~MASK_TEMP)
#define ow_level() (PIN_TEMP & MASK_TEMP)

static _Bool ow_reset(void) {
	ow_low();
		/* 480 - 640 */
	_delay_us(640);
	ow_high();
	_delay_us(2);
	for ( uint8_t c = 0; c < 80; ++c ) {
		if ( !ow_level() ) {
			while ( !ow_level() );
			return true;
		}
		_delay_us(1);
	}

	return false;
}

static void ow_send_bit(_Bool bit) {
	ow_low();
	if ( bit ) {
		_delay_us(5);
		ow_high();
		_delay_us(90);
	} else {
		_delay_us(90);
		ow_high();
		_delay_us(5);
	}
}

static void ow_send(uint8_t b) {
	for ( uint8_t c = 0; c < 8; ++c ) {
		ow_send_bit(b & 1);
		b >>= 1;
	}
}

static _Bool ow_read_bit(void) {
	ow_low();
	_delay_us(2);
	ow_high();
	_delay_us(8);
	_Bool r = ow_level();
	_delay_us(80);

	return r;
}

static uint8_t ow_read(void) {
	uint8_t r = 0;

	for ( uint8_t c = 0; c < 8; ++c ) {
		r >>= 1;
		if ( ow_read_bit() ) {
			r |= 0x80;
		}
	}

	return r;
}

#endif
