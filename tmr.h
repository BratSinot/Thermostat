/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TMR_H_
#define TMR_H_

#include <util/atomic.h>
#include <stdint.h>

typedef uint16_t TIMER_TYPE;

// Calculate the value needed for
// the CTC match value in OCR1A.
#define CTC_MATCH_OVERFLOW UINT8_C((F_CPU / 1000) / 64)

volatile TIMER_TYPE timer1_millis;

/*
	CS12	CS11	CS10	Делитель
	0	0	0	0
	0	0	1	1
	0	1	0	8
	0	1	1	64
	1	0	0	256
	1	0	1	1024
*/

static void init_millis(void) {
		/* CTC mode, Clock/8 */
	TCCR0B |= _BV(WGM12) | _BV(CS11) | _BV(CS10);
		/*
			Load the high byte, then the low byteget_millis
			into the output compare
		*/
	OCR0A = CTC_MATCH_OVERFLOW;
		/* Enable the compare match interrupt */
	TIMSK0 |= _BV(OCIE1A);
		/* Now enable global interrupts */
	sei();
}

ISR (TIMER0_COMPA_vect) {
	timer1_millis++;
}

static TIMER_TYPE get_millis() {
	TIMER_TYPE ret;

	// Ensure this cannot be disrupted
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		ret = timer1_millis;
	}

	return ret;
}

#endif
