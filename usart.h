/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USART_H_
#define USART_H_

#include <stdint.h>

static void usart_init_9600(void) {
	#define BAUD 9600
	#include <util/setbaud.h>
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	#if USE_2X
		UCSR0A |= (1 << U2X0);
	#else
		UCSR0A &= ~(1 << U2X0);
	#endif
}

static uint8_t usart_read_byte(void) {
	//while( (UCSR0A & (1 << RXC0)) == 0);
	if ( (UCSR0A & (1 << RXC0)) == 0 ) {
		return 0xFF;
	} else {
		return UDR0;
	}
	//return UDR0;
}

static void usart_write_byte(uint8_t b) {
	while ( ( (UCSR0A & (1 << UDRE0)) == 0 ) );
	UDR0 = b;
}

static void usart_write_str(char* str) {
	do {
		usart_write_byte(*str);
	} while ( *(++str) != '\0' );
}

static void usart_write_strn(char* str, size_t n) {
	do {
		usart_write_byte(*str);
		str++;
	} while ( --n != 0 );
}

#endif
