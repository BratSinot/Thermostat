/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef I2C_H_
#define I2C_H_

#include <avr/io.h>
#include <compat/twi.h>

// I2C clock in Hz
#define SCL_CLOCK  100000L

// defines the data direction (reading from I2C device) in i2c_start(),i2c_rep_start()
#define I2C_READ    1

// defines the data direction (writing to I2C device) in i2c_start(),i2c_rep_start()
#define I2C_WRITE   0

#ifdef DEBUG
  typedef _Bool I2C_TYPE;
#else
  typedef void I2C_TYPE;
#endif

static void wait_transm(void) {
		/* wait until transmission completed */
	while ( !(TWCR & _BV(TWINT)) );
}

static void send_start_cond(void) {
		/* send START condition */
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);

	wait_transm();
}

static void send_addr(uint8_t addr) {
	// send device address
	TWDR = addr;
	TWCR = _BV(TWINT) | _BV(TWEN);
	wait_transm();
}

static void i2c_init(void) {
		/*
			initialize TWI clock: 100 kHz clock,
								  TWPS = 0 => prescaler = 1
		*/
		/* no prescaler */
	TWSR = 0;
		/* must be > 10 for stable operation */
	TWBR = ( (F_CPU / SCL_CLOCK ) - 16) / 2;
}

static _Bool i2c_start(uint8_t addr) {
	send_start_cond();

		/* check value of TWI Status Register. Mask prescaler bits. */
	uint8_t twst = TW_STATUS & 0xF8;
	if( (twst != TW_START) && (twst != TW_REP_START) ) {
		return true;
	}

	send_addr(addr);

		/* check value of TWI Status Register. Mask prescaler bits. */
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) {
		return true;
	}

	return false;
}

static void i2c_start_wait(uint8_t addr) {
	while ( true ) {
		send_start_cond();

			/* check value of TWI Status Register. Mask prescaler bits. */
		uint8_t twst = TW_STATUS & 0xF8;
		if ( (twst != TW_START) && (twst != TW_REP_START) ) {
			continue;
		}

		send_addr(addr);

			/* check value of TWI Status Register. Mask prescaler bits. */
		twst = TW_STATUS & 0xF8;
		if ( (twst == TW_MT_SLA_NACK ) || (twst == TW_MR_DATA_NACK) ) {
				/* device busy, send stop condition to terminate write operation */
			TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);

				/* wait until stop condition is executed and bus released */
			while ( TWCR & _BV(TWSTO) );

			continue;
		}
		break;
	}
}

static void i2c_stop(void) {
		/* send stop condition */
	TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
		/* wait until stop condition is executed and bus released */
	while ( TWCR & _BV(TWSTO) );
}

static I2C_TYPE i2c_write(uint8_t data) {
		/* send data to the previously addressed device */
	send_addr(data);

#ifdef DEBUG
		/* check value of TWI Status Register. Mask prescaler bits */
	return (TW_STATUS & 0xF8) != TW_MT_DATA_ACK;
#endif
}

#endif
