/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOUND_H_
#define SOUND_H_

#include <stdint.h>

static void init_sound(void) {
	DDR_SPEAK |= MASK_SPEAK;
		/* timer1 configuration (for PWM) */
		/* Clear OC1A/OC1B on compare match */
	TCCR1A |= _BV(COM1B1);
		/* mode 8, PWM, Phase and Frequency Correct (TOP value is ICR1), prescaler(8) */
	TCCR1B |= _BV(WGM13) | _BV(CS11);
}

/*void delay_ms(uint16_t count) {
  while(count--) {
    _delay_ms(1);

  }
}*/
static void play_note(uint8_t note, uint8_t duration) {
	OCR1B = 255;

	ICR1H = (note >> 8);
	ICR1L = note;
	_delay_ms(duration);
	OCR1B = 0;
	_delay_ms(80);
}

#endif
