/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

//#define DEBUG

#define DDR_RELAY   DDRC
#define DDR_ADD     DDRD
#define DDR_SUB     DDRD
#define DDR_TEMP    DDRB
//#define DDR_SPEAK   DDRB

#define PORT_RELAY  PORTC
#define PORT_ADD    PORTD
#define PORT_SUB    PORTD
//#define PORT_SPEAK  PORTB

#define PIN_RELAY   PINC
#define PIN_ADD     PIND
#define PIN_SUB     PIND
#define PIN_TEMP    PINB

	/* Relay - A2 */
#define MASK_RELAY  _BV(PC2)
	/* ADD   - D3 */
#define MASK_ADD    _BV(PD3)
	/* SUB   - D4 */
#define MASK_SUB    _BV(PD4)
	/* TEMP  - D12 */
#define MASK_TEMP   _BV(PB4)
	/* SPEAK - D10 */
//#define MASK_SPEAK  _BV(PB2)


#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <float.h>


#include <avr/io.h>

#include "lcd.h"
#include "ow.h"
#include "tmr.h"
#ifdef DEBUG
	#include "usart.h"
#endif
//#include "eprm.h"
//#include "sound.h"

#define SET_IN(port, bit) do { port |= bit; } while( false );

#define get_act(n, act) ( ((n) & (act)) == (act) )
#define set_act(n, act) do { (n) |= (act); } while ( false );
#define del_act(n, act) { (n) &= (~(act)); }

#define HYSTER_UP	0.5f
#define HYSTER_DOWN	0.3f
#define HEAT()   set_act(PORT_RELAY, MASK_RELAY)
#define FREEZE() del_act(PORT_RELAY, MASK_RELAY)

#define add_push() get_act(PIN_ADD, MASK_ADD)
#define sub_push() get_act(PIN_SUB, MASK_SUB)
static char get_heat(void) {
	char ret = ' ';
	if ( get_act(PIN_RELAY, MASK_RELAY) ) {
		ret = '*';
	}
	return ret;
}

static float Raw2Cel(uint8_t data[9]) {
	int16_t raw = (data[TEMP_MSB] << 8) | data[TEMP_LSB];
	uint8_t cfg = (data[COUNT_REMAIN] & 0x60);

	//raw = raw & ~(7>>(cfg>>5));
	switch ( cfg ) {
		case 0x00:
			raw = raw & ~7;
			break;
		case 0x20:
			raw = raw & ~3;
			break;
		case 0x40:
			raw = raw & ~1;
			break;
	}
	//return (raw - 0.75f - (float)data[COUNT_REMAIN] / data[COUNT_PER_C]) / 16.0f;
	return (float)raw / 16.0f;
}

#define splitFloat(n, i, f)	\
	do {					\
		i = (int8_t)n;		\
		f = (n - i) * 100;	\
	} while ( false );

#define DS18b20_DELAY_94  94
#define DS18b20_MASK_94   0b00011111

#define DS18b20_DELAY_188 188
#define DS18b20_MASK_199  0b00111111

#define DS18b20_DELAY_375 375
#define DS18b20_MASK_375  0b01011111

#define DS18b20_DELAY_750 750
#define DS18b20_MASK_750  0b01111111
static void init_12bit(void) {
	ow_reset();
	ow_send(SKIPROM);
	ow_send(WRITESCRATCH);
	ow_send(0x00);
	ow_send(0x00);
	/*
		R1 R0 RES TIME
		 0  0   9 93.75
		 0  1  10 187.5
		 1  0  11 375
		 1  1  12 750
		0 R1 R0 1 1 1 1 1
	*/
	ow_send(DS18b20_MASK_750);

	ow_reset();
	ow_send(COPYSCRATCH);
	_delay_us(15);
}

static void read_temp(float* tNow, float t, float* min_t, float* max_t) {
	static TIMER_TYPE s = 0;
	if ( s == 0 ) {
		ow_reset();
		ow_send(SKIPROM);
		ow_send(STARTCONVO);
		s = get_millis();
	}

	if ( get_millis() - s >= DS18b20_DELAY_750 ) {
		s = 0;
		ow_reset();
		ow_send(SKIPROM);
		ow_send(READSCRATCH);

		uint8_t data[9];
		for ( uint8_t i = 0; i < 9; i++ ) {
			data[i] = ow_read();
		}

		*tNow = -99.0f;
		if ( ow_crc8(data, 8) == data[SCRATCHPAD_CRC] ) {
			*tNow = Raw2Cel(data);

			if ( *max_t < *tNow && *tNow < 85.0f ) {
				*max_t = *tNow;
			}
			if ( *min_t > *tNow ) {
				*min_t = *tNow;
			}

			if ( *tNow <= t - HYSTER_UP ) {
				HEAT();
			}
			if ( *tNow >= t - HYSTER_DOWN ) {
				FREEZE();
			}
		} else {
			FREEZE();
			//C6 --
			//play_note(956, 10);
		}
	}
}

static void temp2buff( char buff[16], char title, char heat, int8_t ti, uint8_t tf, int8_t m_ti, uint8_t m_tf  ) {
	buff[0] = title;
    buff[1] = ':';
	if ( ti < 0 ) {
		ti = -ti;
		buff[2] = '-';
	} else {
		buff[2] = ' ';
	}
    buff[3] = (ti / 10)  % 10 + '0';
    buff[4] = (ti)       % 10 + '0';
    buff[5] = '.';
    buff[6] = (tf / 10)  % 10 + '0';
    buff[7] = (tf)       % 10 + '0';
    buff[8] = heat;
	if ( m_ti < 0 ) {
		m_ti = -m_ti;
		buff[9] = '-';
	} else {
		buff[9] = ' ';
	}
    buff[10] = (m_ti / 10)  % 10 + '0';
    buff[11] = (m_ti)       % 10 + '0';
    buff[12] = '.';
    buff[13] = (m_tf / 10)  % 10 + '0';
    buff[14] = (m_tf)       % 10 + '0';
	buff[15] = '\0';
}

int main(void) {
/*------init()------*/
	float t = 26.0f,
	      max_t = FLT_MIN,
	      min_t = FLT_MAX;

	#ifdef DEBUG
		usart_init_9600();
	#endif

	init_12bit();

	lcd_init();
	lcd_clr();
	//lcd_nobacklight();

	init_millis();
	//init_sound();

	SET_IN(DDR_RELAY, MASK_RELAY);
	SET_IN(DDR_ADD, MASK_ADD);
	SET_IN(DDR_SUB, MASK_SUB);
	//SET_IN(DDR_SPEAK, MASK_SPEAK);
/*------------------*/

	TIMER_TYPE s = 0;
	float tNow;

	enum act_t {
		EMPTY_ACT	= 0b0,
		ADD_ACT		= 0b01,
		SUB_ACT		= 0b10,
		LED_ACT		= ADD_ACT | SUB_ACT
	};
	register uint8_t button	= EMPTY_ACT;

	while ( true ) {
		read_temp(&tNow, t, &min_t, &max_t);

		char buff[16];
		int8_t ti, m_ti;
		uint8_t tf, m_tf;

		splitFloat(tNow, ti, tf);
		splitFloat(min_t, m_ti, m_tf);

		temp2buff(buff, 'T', get_heat(), ti, tf, m_ti, m_tf);
		lcd_gotoxy(0, 0);
		lcd_puts(buff);

		splitFloat(t, ti, tf);
		splitFloat(max_t, m_ti, m_tf);

		temp2buff(buff, 'M', ' ', ti, tf, m_ti, m_tf);
		lcd_gotoxy(0, 1);
		lcd_puts(buff);

		_Bool _add = add_push(),
		      _sub = sub_push();
		if ( _add ) {
			set_act(button, ADD_ACT);
		}
		if ( _sub ) {
			set_act(button, SUB_ACT);
		}
		if ( !(_add || _sub) ) {
			if ( get_act(button, LED_ACT) ) {
				lcd_backlight();
				del_act(button, LED_ACT);
			}
			else if ( get_act(button, ADD_ACT) ) {
				t += 0.5f;
				del_act(button, ADD_ACT);
			}
			else if ( get_act(button, SUB_ACT) ) {
				t -= 0.5f;
				del_act(button, SUB_ACT);
			}
		}

		#ifdef DEBUG
			uint8_t __debug_b = usart_read_byte();

			switch (__debug_b) {
				case 0xFF:
					break;
				case 'm':
					splitFloat(tNow, ti, tf);
					splitFloat(min_t, m_ti, m_tf);
					temp2buff(buff, 'T', get_heat(), ti, tf, m_ti, m_tf);
					usart_write_str(buff);
					break;
				case 'c':
					splitFloat(t, ti, tf);
					splitFloat(max_t, m_ti, m_tf);
					temp2buff(buff, 'M', ' ', ti, tf, m_ti, m_tf);
					usart_write_str(buff);
					break;
			}
		#endif
	}
}
