/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EEPROM_H_
#define EEPROM_H_

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

#include <stdint.h>

static void eprm_write_byte(uintptr_t addr, uint8_t b) {
	while(!eeprom_is_ready());

	cli();
	eeprom_write_byte((uint8_t *)addr, b);
	sei();
}

static void eprm_write_block(uintptr_t addr, uint8_t* data, size_t len) {
	while(!eeprom_is_ready());

	cli();
	eeprom_write_block(data, (uint8_t *)addr, len);
	sei();
}

#endif
