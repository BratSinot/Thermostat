DEVICE	= atmega328p
CLOCK	= 16000000
#DEV_BLK=ttyACM0
DEV_BLK=ttyUSB0
BAUD=57600
#BAUD=19200
PROGRAMMER=arduino
#PROGRAMMER=wiring

FLAGS=-std=c11 -Os -DF_CPU=$(CLOCK) -mmcu=$(DEVICE)
CFLAGS= $(FLAGS)
CC = avr-gcc
#LIBS=-Wl,-u,vfprintf -lprintf_flt -lm

all: obj asm
	$(CC) $(CFLAGS) -o thermostat.elf  thermostat.o $(LIBS)
	avr-objcopy -j .text -j .data -O ihex thermostat.elf thermostat.hex
	avr-size --format=avr --mcu=$(DEVICE) thermostat.elf

obj:
	$(CC) -g $(CFLAGS) -c thermostat.c -o thermostat.o $(LIBS)

asm:
	$(CC) -g $(FLAGS) -S -c thermostat.c -o thermostat.S $(LIBS)

flash: all
	sudo avrdude -C/opt/arduino/hardware/tools/avr/etc/avrdude.conf -v -p$(DEVICE) -c$(PROGRAMMER) -P/dev/$(DEV_BLK) -b$(BAUD) -D -Uflash:w:thermostat.hex:i

backup:
	sudo avrdude  -C/opt/arduino/hardware/tools/avr/etc/avrdude.conf -v -p$(DEVICE) -c$(PROGRAMMER) -P/dev/$(DEV_BLK) -b$(BAUD) -Uflash:r:'printer_firmware'.hex:i

clean:
	rm -f thermostat.o thermostat.hex thermostat.S thermostat.elf
