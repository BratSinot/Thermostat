/*
	This file is part of Thermostate.

	Copyright (C) 2019-2020  DolphinCommode

	Thermostate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Thermostate is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Thermostate.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LCD_H_
#define LCD_H_

#include <avr/pgmspace.h>
#include <util/delay.h>

#include "i2c.h"

// PCF8574
#define P7	7
#define P6	6
#define P5	5
#define P4	4
#define P3	3
#define P2	2
#define P1	1
#define P0	0

#define PCF8574				0x7E

#define LCD_RS_PIN			P0	// LCD-Pin RS is connected to P0 on the PCF8574
#define LCD_RW_PIN			P1	// LCD-Pin RW is connected to P1 on the PCF8574
#define LCD_E_PIN			P2	// LCD-Pin E is connected to P2 on the PCF8574
#define LCD_BLIGHT_PIN		P3	// LCD-BACKLIGHT pin is connected to P3 on the PCF8574

#define LCD_RS				_BV(LCD_RS_PIN)
#define LCD_RW				_BV(LCD_RW_PIN)
//#define LCD_BLIGHT		_BV(LCD_BLIGHT_PIN)
uint8_t LCD_BLIGHT =		_BV(LCD_BLIGHT_PIN);
#define LCD_E				_BV(LCD_E_PIN)

#define LCD_CLEAR			0	//clear display
#define LCD_HOME			1	//return to home position
#define LCD_CGRAM			6	//set CG RAM address
#define LCD_DDRAM			7	//set DD RAM address

#define LCD_ENTRYMODE			0x04			// Set entrymode
	#define LCD_INCREASE		LCD_ENTRYMODE | 0x02	// Set cursor move direction -- Increase
	#define LCD_DECREASE		LCD_ENTRYMODE | 0x00	// Set cursor move direction -- Decrease
	#define LCD_DISPLAYSHIFTON	LCD_ENTRYMODE | 0x01	// Display is shifted
	#define LCD_DISPLAYSHIFTOFF	LCD_ENTRYMODE | 0x00	// Display is not shifted

#define LCD_DISPLAYMODE			0x08			// Set displaymode
	#define LCD_DISPLAYON		LCD_DISPLAYMODE | 0x04	// Display on
	#define LCD_DISPLAYOFF		LCD_DISPLAYMODE | 0x00	// Display off
	#define LCD_CURSORON		LCD_DISPLAYMODE | 0x02	// Cursor on
	#define LCD_CURSOROFF		LCD_DISPLAYMODE | 0x00	// Cursor off
	#define LCD_BLINKINGON		LCD_DISPLAYMODE | 0x01	// Blinking on
	#define LCD_BLINKINGOFF		LCD_DISPLAYMODE | 0x00	// Blinking off

#define LCD_SHIFTMODE			0x10			// Set shiftmode
	#define LCD_DISPLAYSHIFT	LCD_SHIFTMODE | 0x08	// Display shift
	#define LCD_CURSORMOVE		LCD_SHIFTMODE | 0x00	// Cursor move
	#define LCD_RIGHT		LCD_SHIFTMODE | 0x04	// Right shift
	#define LCD_LEFT		LCD_SHIFTMODE | 0x00	// Left shift

#define LCD_CONFIGURATION		0x20				// Set function
	#define LCD_8BIT		LCD_CONFIGURATION | 0x10	// 8 bits interface
	#define LCD_4BIT		LCD_CONFIGURATION | 0x00	// 4 bits interface
	#define LCD_2LINE		LCD_CONFIGURATION | 0x08	// 2 line display
	#define LCD_1LINE		LCD_CONFIGURATION | 0x00	// 1 line display
	#define LCD_5X10		LCD_CONFIGURATION | 0x04	// 5 X 10 dots
	#define LCD_5X7			LCD_CONFIGURATION | 0x00	// 5 X 7 dots

// cursor position to DDRAM mapping
#define LCD_LINE0_DDRAMADDR		0x00
#define LCD_LINE1_DDRAMADDR		0x40

#define STROBE_EN( data )	\
	do {					\
		data |= LCD_E;		\
		lcd_pio(data);		\
		data &= ~LCD_E;		\
		lcd_pio(data);		\
	} while(false);

static void lcd_pio(uint8_t data) {
	i2c_start_wait(PCF8574 + I2C_WRITE);
	i2c_write(data + LCD_BLIGHT);
	i2c_stop();
}

static void lcd_cmd(uint8_t cmd) {
	uint8_t lcd_data = (cmd & 0xF0);
	lcd_data &= ~LCD_RS;
	STROBE_EN( lcd_data );
	lcd_pio(lcd_data);
	_delay_us(100);

	lcd_data = (cmd & 0x0F) << 4;
	lcd_data &= ~LCD_RS;
	STROBE_EN( lcd_data );
	lcd_pio(lcd_data);

	if ( cmd & 0b11111100 ) {
		_delay_us(100);
	} else {
		_delay_ms(2);
	}
}

static void lcd_putch(uint8_t chr)  {
	uint8_t lcd_data = (chr & 0xF0);
	lcd_data |= LCD_RS;
	STROBE_EN( lcd_data );
	_delay_us(100);

	lcd_data = (chr & 0x0F) << 4;
	lcd_data |= LCD_RS;
	STROBE_EN( lcd_data );
	lcd_pio(lcd_data);
	_delay_ms(2);
}

static void lcd_init() {
	i2c_init();

	lcd_pio(0b00000000);
	_delay_ms(40);
		/* 3 x 0x03h, 8bit */
	uint8_t lcd_data = 0b00110000;
	lcd_pio(lcd_data);
	STROBE_EN( lcd_data );
		/* delay > 4,1ms */
	_delay_ms(5);
	STROBE_EN( lcd_data );
	_delay_us(100);
	STROBE_EN( lcd_data );
	_delay_us(100);
		/* 4bit - 0x02h */
	lcd_data = 0b00100000;
	lcd_pio(lcd_data);
	STROBE_EN( lcd_data );
	lcd_pio(lcd_data);
	_delay_us(100);
		/* From now on in 4-bit-Mode */
		/*- 2-Lines, 5x7-Matrix */
	lcd_cmd(LCD_4BIT | LCD_2LINE | LCD_5X7);
		/*- Display off */
	lcd_cmd(LCD_DISPLAYOFF);
		/*- Clear Screen */
	lcd_cmd(LCD_CLEAR);
		/*- Entrymode (Display Shift: off, Increment Address Counter) */
	lcd_cmd(LCD_INCREASE | LCD_DISPLAYSHIFTOFF);
		/*- Display on */
	lcd_cmd(LCD_DISPLAYON | LCD_BLINKINGOFF);
}

static void lcd_gotoxy(uint8_t x, uint8_t y) {
	register uint8_t DDRAM_addr;
		/* remap lines into proper order */
	switch ( y ) {
		case 0:
			DDRAM_addr = LCD_LINE0_DDRAMADDR + x;
			break;
		case 1:
			DDRAM_addr = LCD_LINE1_DDRAMADDR + x;
			break;
		default:
			DDRAM_addr = LCD_LINE0_DDRAMADDR + x;
	}
		/* set data address */
	lcd_cmd( _BV(LCD_DDRAM) | DDRAM_addr);
}

void lcd_puts(const char *s) {
	while ( *s ) {
		lcd_putch(*s++);
	}
}

#define lcd_P(str) lcd_pgmputs(PSTR(str));
static void lcd_pgmputs(const char *progmem_s) {
	register char c;
	while ( c = pgm_read_byte(progmem_s++) ) {
		lcd_putch(c);
	}
}

#define lcd_clr()  lcd_cmd( _BV(LCD_CLEAR) )
#define lcd_home() lcd_cmd( _BV(LCD_HOME) )
#define lcd_backlight() do { LCD_BLIGHT ^= _BV(P3); } while ( false )

/*void lcd_backlight(void) {
	LCD_BLIGHT = _BV(P3);
}

void lcd_nobacklight(void) {
	LCD_BLIGHT = 0;
}*/

#endif
